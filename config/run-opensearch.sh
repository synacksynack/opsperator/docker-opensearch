#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
umask 0002
RUNTIME_USER=opensearch
. /usr/local/bin/nsswrapper.sh

if test -z "$HOSTNAME"; then
    HOSTNAME=`hostname`
fi
OPENSEARCH_CLUSTER_NAME=${OPENSEARCH_CLUSTER_NAME:-k8s}
OPENSEARCH_DATA_DIR=${OPENSEARCH_DATA_DIR:-/var/lib/opensearch}
OPENSEARCH_HOME=${OPENSEARCH_HOME:-/usr/share/opensearch}
OPENSEARCH_HAS_DATA=${OPENSEARCH_HAS_DATA:-1}
OPENSEARCH_HAS_MASTERS=${OPENSEARCH_HAS_MASTERS:-1}
OPENSEARCH_JVM_XMS=${OPENSEARCH_JVM_XMS:-512m}
OPENSEARCH_JVM_XMX=${OPENSEARCH_JVM_XMX:-512m}
OPENSEARCH_SERVER_ID=`echo "$HOSTNAME" | sed 's|^.*-\([0-9][0-9]*\)$|\1|'`

if test -z "$POD_IP"; then
    POD_IP=$(cat /proc/net/fib_trie | grep "|--"  \
		| egrep -v "(0.0.0.0| 127.|.255$|.0$)" \
		| awk '{print $NF}' | head -1)
fi

gen_masters()
{
    local check

    check=0
    while test "$check" -lt "$OPENSEARCH_HAS_MASTERS"
    do
	if test "$OPENSEARCH_SEEDS"; then
	    export OPENSEARCH_SEEDS="$OPENSEARCH_SEEDS
- $OPENSEARCH_MASTER_SVC-$check.$OPENSEARCH_MASTER_SVC"
	else
	    export OPENSEARCH_SEEDS="- $OPENSEARCH_MASTER_SVC-$check.$OPENSEARCH_MASTER_SVC"
	fi
	check=`expr $check + 1`
    done
}

if test -z "$OPENSEARCH_IS_MASTER" -a -z "$OPENSEARCH_IS_DATA" \
	-a -z "$OPENSEARCH_IS_INGEST"; then
    OPENSEARCH_IS_DATA=true
    OPENSEARCH_IS_INGEST=true
    OPENSEARCH_IS_MASTER=true
else
    for k in MASTER DATA INGEST
    do
	eval "v=\$OPENSEARCH_IS_$k"
	if echo "$v" | grep -E '^(t|y)' >/dev/null; then
	    eval "OPENSEARCH_IS_$k=true"
	else
	    eval "OPENSEARCH_IS_$k=false"
	fi
    done
fi

if ! test "$OPENSEARCH_SERVER_ID" = "$HOSTNAME"; then
    OPENSEARCH_BASENAME=`echo $HOSTNAME | sed "s|-$OPENSEARCH_SERVER_ID$||"`
    OPENSEARCH_MASTER_SVC=$OPENSEARCH_BASENAME
fi
if test "$OPENSEARCH_MASTER_SVC"; then
    gen_masters
    if $OPENSEARCH_IS_MASTER; then
	OPENSEARCH_NODENAME=$OPENSEARCH_MASTER_SVC-$OPENSEARCH_SERVER_ID.$OPENSEARCH_MASTER_SVC
    else
	OPENSEARCH_NODENAME=$HOSTNAME
    fi
else
    export OPENSEARCH_SEEDS="- $HOSTNAME"
    OPENSEARCH_NODENAME=$HOSTNAME
fi
if test -z "$OPENSEARCH_MAX_DATA"; then
    OPENSEARCH_MAX_DATA=$OPENSEARCH_HAS_DATA
fi

sed -e "s|CLUSTER_NAME|$OPENSEARCH_CLUSTER_NAME|" \
    -e "s|DATA_DIR|$OPENSEARCH_DATA_DIR|" \
    -e "s|MAXIMUM_DATA|$OPENSEARCH_MAX_DATA|" \
    -e "s|NODE_NAME|$OPENSEARCH_NODENAME|" \
    /opensearch.yml \
    >$OPENSEARCH_HOME/config/opensearch.yml

cat <<EOF >>$OPENSEARCH_HOME/config/opensearch.yml
cluster.initial_cluster_manager_nodes:
$OPENSEARCH_SEEDS
discovery.seed_hosts:
$OPENSEARCH_SEEDS
node.roles:
EOF

if "$OPENSEARCH_IS_MASTER"; then
    cat <<EOF >>$OPENSEARCH_HOME/config/opensearch.yml
- cluster_manager
EOF
fi
if "$OPENSEARCH_IS_INGEST"; then
    cat <<EOF >>$OPENSEARCH_HOME/config/opensearch.yml
- ingest
EOF
fi
if "$OPENSEARCH_IS_DATA"; then
    cat <<EOF >>$OPENSEARCH_HOME/config/opensearch.yml
- data
EOF
fi

if test "$OPENSEARCH_DO_CORS"; then
    cat <<EOF >>$OPENSEARCH_HOME/config/opensearch.yml
http.cors.enabled: true
http.cors.allow-origin: "*"
EOF
fi

sed -e "s|XMS|$OPENSEARCH_JVM_XMS|" \
    -e "s|XMX|$OPENSEARCH_JVM_XMX|" \
    /jvm.options \
    >$OPENSEARCH_HOME/config/jvm.options

TCK=`getconf CLK_TCK`
echo "-Dclk.tck=$TCK" >>$OPENSEARCH_HOME/config/jvm.options

if test "$OPENSEARCH_PLUGINS"; then
    for plugin in $OPENSEARCH_PLUGINS
    do
	if ! test -d $OPENSEARCH_HOME/plugins/$plugin; then
	    echo y | \
		$OPENSEARCH_HOME/bin/opensearch-plugin \
		install $plugin
	fi
    done
fi

cat <<EOF
Starting OpenSearch node
is_data:$OPENSEARCH_IS_DATA is_ingest:$OPENSEARCH_IS_INGEST is_master:$OPENSEARCH_IS_MASTER
xms:$OPENSEARCH_JVM_XMS xmx:$OPENSEARCH_JVM_XMX
EOF
if test "$DEBUG"; then
    cat $OPENSEARCH_HOME/config/opensearch.yml
fi

unset OPENSEARCH_CLUSTER_NAME OPENSEARCH_DATA_DIR OPENSEARCH_HAS_MASTERS \
    OPENSEARCH_JVM_XMS OPENSEARCH_JVM_XMX OPENSEARCH_PING_INTERVAL TCK \
    OPENSEARCH_PING_RETRIES OPENSEARCH_PING_TIMEOUT OPENSEARCH_SERVER_ID \
    OPENSEARCH_IS_DATA OPENSEARCH_IS_MASTER OPENSEARCH_IS_INGEST POD_IP \
    OPENSEARCH_MASTER_SVC OPENSEARCH_BASENAME OPENSEARCH_NODENAME

exec "$@"
