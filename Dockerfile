FROM opsperator/java

ARG DO_UPGRADE=
ARG GID=1001
ARG UID=1001
ENV BASE_URL=https://artifacts.opensearch.org/releases/bundle/opensearch \
    OPENSEARCH_HOME=/usr/share/opensearch \
    OPENSEARCH_TMPDIR=/tmp \
    PATH="/usr/share/opensearch/bin:$PATH" \
    DISABLE_INSTALL_DEMO_CONFIG=true \
    DISABLE_SECURITY_PLUGIN=false \
    VERSION=2.2.0

LABEL io.k8s.description="OpenSearch" \
      io.k8s.display-name="OpenSearch $VERSION" \
      io.openshift.expose-services="9200:opensearch,9300:nodes,9600:perfagent,9650:perfdiag" \
      io.openshift.tags="opensearch" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-opensearch" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$VERSION"

USER root
COPY config/* /

RUN set -x \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install OpenSearch Dependencies" \
    && apt-get install -y --no-install-recommends wget \
	zip unzip procps findutils curl \
    && OSARCH=`uname -m` \
    && if ! echo "$OSARCH" | grep -E '(x86_64|aarch64)' >/dev/null; then \
	echo CRITICAL: unsupported architecture \
	&& exit 1; \
    fi \
    && if echo "$OSARCH" | grep aarch64; then \
	curl -fsL -o /tmp/os.tar.gz \
	    $BASE_URL/$VERSION/opensearch-$VERSION-linux-arm64.tar.gz; \
    else \
	curl -fsL -o /tmp/os.tar.gz \
	     $BASE_URL/$VERSION/opensearch-$VERSION-linux-x64.tar.gz; \
    fi \
    && echo "maybe do groupadd -g $GID opensearch" \
    && echo "maybe do adduser -u $UID -g $GID -d $OPENSEARCH_HOME opensearch" \
    && mkdir -p /var/lib/opensearch /usr/share/opensearch \
    && mv /escli /usr/local/bin/ \
    && tar -xzf /tmp/os.tar.gz -C $OPENSEARCH_HOME --strip-components=1 \
    && rm -fr /tmp/os.tar.gz $OPENSEARCH_HOME/jdk $OPENSEARCH_HOME/data \
    && ln -sf /opt/jdk $OPENSEARCH_HOME/jdk \
    && ln -sf /var/lib/opensearch $OPENSEARCH_HOME/data \
    && mv /log4j2.properties $OPENSEARCH_HOME/config/ \
    && if test -d $OPENSEARCH_HOME/config/opensearch-performance-analyzer/; then \
	mv /performance-analyzer.properties \
	    $OPENSEARCH_HOME/config/opensearch-performance-analyzer/; \
    else \
	rm -f /performance-analyzer.properties; \
    fi \
    && chmod +x $OPENSEARCH_HOME/plugins/opensearch-security/tools/* \
    && echo "# Fixing Permissions" \
    && chown -R $UID:0 $OPENSEARCH_HOME/config $OPENSEARCH_HOME/logs \
	$OPENSEARCH_HOME/plugins $OPENSEARCH_HOME/modules /var/lib/opensearch \
    && chmod -R g=u $OPENSEARCH_HOME/config $OPENSEARCH_HOME/logs \
	$OPENSEARCH_HOME/plugins $OPENSEARCH_HOME/modules /var/lib/opensearch \
    && echo "# Cleaning Up" \
    && apt-get clean -y \
    && apt-get autoremove -y --purge \
    && rm -rvf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ENV LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$OPENSEARCH_HOME/plugins/opensearch-knn/knnlib"
USER $UID
WORKDIR $OPENSEARCH_HOME
ENTRYPOINT ["dumb-init","--","/run-opensearch.sh"]
CMD "/usr/share/opensearch/bin/opensearch"
