# k8s OpenSearch

OpenShift-friendly OpenSearch Image

Building Clusters, make sure to set `OPENSEARCH_HAS_MASTERS` to the minimum
amount of Pods our masters statefulset would hold. Although note that whatever
value would work, it would be required ensuring proper nodes detection during
an outage.

The provided template is merely an example of how OpenSearch can be
deployed into OpenShift. We could use a single statefulset hosting
masters and data nodes as well, although moving data nodes away seems
more fitting scaling out specific services. Ingest nodes could be moved
to a separate statefulset as well.

Environment variables
----------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name                |    Description                          | Default                    |
| :------------------------------ | --------------------------------------- | -------------------------- |
|  `OPENSEARCH_CLUSTER_NAME`      | OpenSearch Cluster Name                 | `k8s`                      |
|  `OPENSEARCH_DATA_DIR`          | OpenSearch Data Directory               | `/var/lib/opensearch`      |
|  `OPENSEARCH_DO_CORS`           | OpenSearch Toggles Cors Configuration   | undef                      |
|  `OPENSEARCH_HAS_DATA`          | OpenSearch Expected Data Nodes Count    | `1`                        |
|  `OPENSEARCH_HAS_MASTERS`       | OpenSearch Expected Masters Nodes Count | `1`                        |
|  `OPENSEARCH_IS_DATA`           | OpenSearch Works as Data Node           | undef                      |
|  `OPENSEARCH_IS_INGEST`         | OpenSearch Works as Ingest Node         | undef                      |
|  `OPENSEARCH_IS_MASTER`         | OpenSearch Works as Master Node         | undef                      |
|  `OPENSEARCH_JVM_XMS`           | OpenSearch JVM XMS Option               | `512m`                     |
|  `OPENSEARCH_JVM_XMX`           | OpenSearch JVM XMX Option               | `512m`                     |
|  `OPENSEARCH_MAX_DATA`          | OpenSearch Maximum Data Nodes           | deduced from `HAS_DATA`    |
|  `OPENSEARCH_PLUGINS`           | OpenSearch Plugins to Install           | undef                      |
|  `POD_IP`                       | OpenSearch Listen Address               | detected                   |

Knowing that if none of `IS_MASTER`, `IS_INGEST` nor `IS_DATA` were set, then
all three would default to `true`.

Volumes
--------

|  Volume mount point        | Description                |
| :------------------------- | :------------------------- |
|  `/var/lib/opensearch`     | OpenSearch Data Directory  |
