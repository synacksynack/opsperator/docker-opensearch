apiVersion: v1
kind: Template
labels:
  app: opensearch
  template: opensearch-statefulset-persistent
metadata:
  annotations:
    description: OpenSearch Cluster
    iconClass: icon-openshift
    openshift.io/display-name: OpenSearch Cluster
    tags: opensearch
  name: opensearch-statefulset-persistent
objects:
- apiVersion: apps/v1
  kind: StatefulSet
  metadata:
    name: opensearch-master-${FRONTNAME}
  spec:
    podManagementPolicy: Parallel
    replicas: ${OPENSEARCH_MASTERS_REPLICA}
    selector:
      matchLabels:
        opensearch: ${FRONTNAME}
        name: opensearch-master-${FRONTNAME}
    serviceName: opensearch-master-${FRONTNAME}
    template:
      metadata:
        labels:
          opensearch: ${FRONTNAME}
          name: opensearch-master-${FRONTNAME}
          tuned.openshift.io/opensearch: ""
      spec:
        affinity:
          podAntiAffinity:
            preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                  - key: name
                    operator: In
                    values: [ "opensearch-master-${FRONTNAME}" ]
                topologyKey: kubernetes.io/hostname
              weight: 42
        containers:
        - env:
          - name: OPENSEARCH_CLUSTER_NAME
            value: ${FRONTNAME}
          - name: OPENSEARCH_HAS_DATA
            value: "${OPENSEARCH_DATA_REPLICA}"
          - name: OPENSEARCH_HAS_MASTERS
            value: "${OPENSEARCH_MASTERS_REPLICA}"
          - name: OPENSEARCH_IS_MASTER
            value: yay
          - name: OPENSEARCH_JVM_XMS
            value: "${OPENSEARCH_MASTER_XMS}"
          - name: OPENSEARCH_JVM_XMX
            value: "${OPENSEARCH_MASTER_XMX}"
          - name: POD_IP
            valueFrom:
              fieldRef:
                fieldPath: status.podIP
          image: ${OPENSHIFT_REGISTRY}/${BUILDS}/opensearch:${OPENSEARCH_IMAGE_TAG}
          imagePullPolicy: Always
          livenessProbe:
            failureThreshold: 20
            initialDelaySeconds: 30
            periodSeconds: 30
            timeoutSeconds: 5
            tcpSocket:
              port: 9200
          name: opensearch
          ports:
          - name: esearch
            containerPort: 9200
          - name: nodes
            containerPort: 9300
          readinessProbe:
            exec:
              command:
              - /bin/sh
              - "-i"
              - "-c"
              - /is-ready.sh
            initialDelaySeconds: 40
            periodSeconds: 10
            timeoutSeconds: 5
          resources:
            limits:
              cpu: "${OPENSEARCH_MASTER_CPU_LIMIT}"
              memory: "${OPENSEARCH_MASTER_MEMORY_LIMIT}"
          volumeMounts:
          - name: data
            mountPath: /var/lib/opensearch
    volumeClaimTemplates:
    - metadata:
        name: data
      spec:
        accessModes: [ ReadWriteOnce ]
        resources:
          requests:
            storage: ${OPENSEARCH_MASTER_VOLUME_CAPACITY}
- apiVersion: v1
  kind: Service
  metadata:
    annotations:
      service.alpha.kubernetes.io/tolerate-unready-endpoints: "true"
    name: opensearch-master-${FRONTNAME}
  spec:
    ports:
    - name: esearch
      protocol: TCP
      port: 9200
      targetPort: 9200
    - name: nodes
      protocol: TCP
      port: 9300
      targetPort: 9300
    selector:
      name: opensearch-master-${FRONTNAME}
    clusterIP: None
- apiVersion: apps/v1
  kind: StatefulSet
  metadata:
    name: opensearch-data-${FRONTNAME}
  spec:
    podManagementPolicy: Parallel
    replicas: ${OPENSEARCH_DATA_REPLICA}
    selector:
      matchLabels:
        opensearch: ${FRONTNAME}
        name: opensearch-data-${FRONTNAME}
    serviceName: opensearch-data-${FRONTNAME}
    template:
      metadata:
        labels:
          opensearch: ${FRONTNAME}
          name: opensearch-data-${FRONTNAME}
          tuned.openshift.io/opensearch: ""
      spec:
        affinity:
          podAntiAffinity:
            preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                  - key: name
                    operator: In
                    values: [ "opensearch-data-${FRONTNAME}" ]
                topologyKey: kubernetes.io/hostname
              weight: 42
        containers:
        - env:
          - name: OPENSEARCH_CLUSTER_NAME
            value: ${FRONTNAME}
          - name: OPENSEARCH_HAS_DATA
            value: "${OPENSEARCH_DATA_REPLICA}"
          - name: OPENSEARCH_HAS_MASTERS
            value: "${OPENSEARCH_MASTERS_REPLICA}"
          - name: OPENSEARCH_IS_DATA
            value: yay
          - name: OPENSEARCH_IS_INGEST
            value: yay
          - name: OPENSEARCH_JVM_XMS
            value: "${OPENSEARCH_DATA_XMS}"
          - name: OPENSEARCH_JVM_XMX
            value: "${OPENSEARCH_DATA_XMX}"
          - name: OPENSEARCH_MASTER_SVC
            value: opensearch-master-${FRONTNAME}
          - name: POD_IP
            valueFrom:
              fieldRef:
                fieldPath: status.podIP
          image: ${OPENSHIFT_REGISTRY}/${BUILDS}/opensearch:${OPENSEARCH_IMAGE_TAG}
          imagePullPolicy: Always
          livenessProbe:
            failureThreshold: 20
            initialDelaySeconds: 30
            periodSeconds: 30
            timeoutSeconds: 5
            tcpSocket:
              port: 9200
          name: opensearch
          ports:
          - name: esearch
            containerPort: 9200
          - name: nodes
            containerPort: 9300
          readinessProbe:
            exec:
              command:
              - /bin/sh
              - "-i"
              - "-c"
              - /is-ready.sh
            initialDelaySeconds: 40
            periodSeconds: 10
            timeoutSeconds: 5
          resources:
            limits:
              cpu: "${OPENSEARCH_DATA_CPU_LIMIT}"
              memory: "${OPENSEARCH_DATA_MEMORY_LIMIT}"
          terminationMessagePath: /dev/termination-log
          volumeMounts:
          - name: data
            mountPath: /var/lib/opensearch
    volumeClaimTemplates:
    - metadata:
        name: data
      spec:
        accessModes: [ ReadWriteOnce ]
        resources:
          requests:
            storage: ${OPENSEARCH_DATA_VOLUME_CAPACITY}
- apiVersion: v1
  kind: Service
  metadata:
    annotations:
      service.alpha.kubernetes.io/tolerate-unready-endpoints: "true"
    name: opensearch-data-${FRONTNAME}
  spec:
    clusterIP: None
    ports:
    - name: esearch
      protocol: TCP
      port: 9200
      targetPort: 9200
    - name: nodes
      protocol: TCP
      port: 9300
      targetPort: 9300
    selector:
      name: opensearch-data-${FRONTNAME}
- apiVersion: v1
  kind: Service
  metadata:
    name: opensearch-clients-${FRONTNAME}
  spec:
    ports:
    - name: esearch
      protocol: TCP
      port: 9200
      targetPort: 9200
    selector:
      opensearch: ${FRONTNAME}
parameters:
- name: OPENSEARCH_DATA_CPU_LIMIT
  description: Maximum amount of CPU an OpenSearch Data container can use
  displayName: OpenSearch Data CPU Limit
  required: true
  value: 300m
- name: OPENSEARCH_DATA_MEMORY_LIMIT
  description: Maximum amount of memory an OpenSearch Data container can use
  displayName: OpenSearch Data Memory Limit
  required: true
  value: 1280Mi
- name: OPENSEARCH_DATA_REPLICA
  value: "2"
- name: OPENSEARCH_DATA_XMS
  value: 768m
- name: OPENSEARCH_DATA_XMX
  value: 768m
- name: OPENSEARCH_DATA_VOLUME_CAPACITY
  description: Volume space available for OpenSearch database, e.g. 512Mi, 2Gi.
  displayName: OpenSearch Data Volume Capacity
  required: true
  value: 8Gi
- name: OPENSEARCH_IMAGE_TAG
  description: The ImageStreamTag we should pull images from
  displayName: Tag
  required: true
  value: master
- name: OPENSEARCH_MASTER_CPU_LIMIT
  description: Maximum amount of CPU an OpenSearch Master container can use
  displayName: OpenSearch Master CPU Limit
  required: true
  value: 300m
- name: OPENSEARCH_MASTER_MEMORY_LIMIT
  description: Maximum amount of memory an OpenSearch Master container can use
  displayName: OpenSearch Master Memory Limit
  required: true
  value: 768Mi
- name: OPENSEARCH_MASTER_XMS
  value: 512m
- name: OPENSEARCH_MASTER_XMX
  value: 512m
- name: OPENSEARCH_MASTER_VOLUME_CAPACITY
  description: Volume space available for OpenSearch masters, e.g. 512Mi, 2Gi.
  displayName: OpenSearch Data Volume Capacity
  required: true
  value: 2Gi
- name: OPENSEARCH_MASTERS_REPLICA
  value: "3"
- name: OPENSHIFT_REGISTRY
  description: OpenShift Registry
  displayName: Registry Address
  required: true
  value: "docker-registry.default.svc:5000"
- name: BUILDS
  description: CI Namespace
  displayName: Builds
  required: true
  value: ci
- name: FRONTNAME
  description: The name identifier assigned to objects defined in this template
  displayName: Name
  required: true
  value: demo
