apiVersion: v1
kind: Template
labels:
  app: opensearch
  template: opensearch-persistent
metadata:
  annotations:
    description: OpenSearch database - persistent
    iconClass: icon-openshift
    openshift.io/display-name: OpenSearch
    tags: opensearch
  name: opensearch-persistent
objects:
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: opensearch-${FRONTNAME}
  spec:
    accessModes:
    - ReadWriteOnce
    resources:
      requests:
        storage: ${OPENSEARCH_DATA_VOLUME_CAPACITY}
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    annotations:
      security.alpha.kubernetes.io/sysctls: vm.max_map_count=262144
    labels:
      name: opensearch-${FRONTNAME}
    name: opensearch-${FRONTNAME}
  spec:
    replicas: 1
    selector:
      name: opensearch-${FRONTNAME}
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: opensearch-${FRONTNAME}
          tuned.openshift.io/opensearch: ""
      spec:
        dnsPolicy: ClusterFirst
        containers:
        - env:
          - name: OPENSEARCH_CLUSTER_NAME
            value: ${FRONTNAME}
          - name: OPENSEARCH_JVM_XMS
            value: "${OPENSEARCH_XMS}"
          - name: OPENSEARCH_JVM_XMX
            value: "${OPENSEARCH_XMX}"
          - name: POD_IP
            valueFrom:
              fieldRef:
                fieldPath: status.podIP
          image: ' '
          imagePullPolicy: IfNotPresent
          livenessProbe:
            failureThreshold: 20
            initialDelaySeconds: 30
            periodSeconds: 30
            timeoutSeconds: 5
            tcpSocket:
              port: 9200
          name: opensearch
          ports:
          - name: esearch
            containerPort: 9200
          - name: nodes
            containerPort: 9300
          readinessProbe:
            exec:
              command:
              - /bin/sh
              - "-i"
              - "-c"
              - /is-ready.sh
            initialDelaySeconds: 40
            periodSeconds: 10
            timeoutSeconds: 5
          resources:
            limits:
              cpu: "${OPENSEARCH_CPU_LIMIT}"
              memory: "${OPENSEARCH_MEMORY_LIMIT}"
          volumeMounts:
          - name: data
            mountPath: /var/lib/opensearch
        restartPolicy: Always
        volumes:
        - name: data
          persistentVolumeClaim:
            claimName: opensearch-${FRONTNAME}
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - opensearch
        from:
          kind: ImageStreamTag
          name: opensearch:${OPENSEARCH_IMAGE_TAG}
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: opensearch-${FRONTNAME}
  spec:
    ports:
    - name: esearch
      protocol: TCP
      port: 9200
      targetPort: 9200
    - name: nodes
      protocol: TCP
      port: 9300
      targetPort: 9300
    selector:
      name: opensearch-${FRONTNAME}
    type: ClusterIP
parameters:
- name: OPENSEARCH_CPU_LIMIT
  description: Maximum amount of CPU an OpenSearch container can use
  displayName: OpenSearch CPU Limit
  required: true
  value: 300m
- name: OPENSEARCH_DATA_VOLUME_CAPACITY
  description: Volume space available for OpenSearch database, e.g. 512Mi, 2Gi.
  displayName: OpenSearch Data Volume Capacity
  required: true
  value: 8Gi
- name: OPENSEARCH_IMAGE_TAG
  description: The ImageStreamTag we should pull images from
  displayName: Tag
  required: true
  value: master
- name: OPENSEARCH_MEMORY_LIMIT
  description: Maximum amount of memory an OpenSearch container can use
  displayName: OpenSearch Memory Limit
  required: true
  value: 1300Mi
- name: OPENSEARCH_XMS
  value: 768m
- name: OPENSEARCH_XMX
  value: 768m
- name: FRONTNAME
  description: The name identifier assigned to objects defined in this template
  displayName: Name
  required: true
  value: demo
